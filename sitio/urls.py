from django.conf.urls.defaults import *
from django.conf import settings

urlpatterns = patterns('pythoni.sitio.views',
    #(r'^index/$', 'index'),
    (r'^tutoriales/$', 'pagina_tutoriales'),
    (r'^tutoriales/(?P<slug>[\w-]+)/$', 'ver_tuto'),
    (r'^tutoriales/', include('django.contrib.comments.urls')),
    
)
