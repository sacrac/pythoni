from django.conf.urls.defaults import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from os import path as os_path
from settings import *

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'pythoni.views.home', name='home'),
    # url(r'^pythoni/', include('pythoni.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    #(r'^comments/', include('django.contrib.comments.urls')),
    (r'^accounts/', include('registration.urls')),
    (r'^$', 'sitio.views.index'),
    (r'^', include('sitio.urls')),
    (r'^', include('evento.urls')),
    (r'^tagging_autocomplete/', include('tagging_autocomplete.urls')),
)

handler404 = 'pythoni.views.file_not_found_404'

handler500 = 'pythoni.views.file_not_found_500'

urlpatterns += staticfiles_urlpatterns()

if DEBUG:
    urlpatterns += patterns('',
                (r'^uploads/(?P<path>.*)$', 'django.views.static.serve', {'document_root': MEDIA_ROOT}),
                )
