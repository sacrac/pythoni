# -*- coding: utf-8 -*-

from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from pythoni.utils import get_file_path
from django.contrib.comments.moderation import CommentModerator, moderator, AlreadyModerated
from django.contrib.comments.signals import comment_was_posted
from django.contrib.comments.models import Comment
from sitio.signals import comment_notifier
#Agregando los tags
from south.modelsinspector import add_introspection_rules
from tagging.models import Tag
from tagging_autocomplete.models import TagAutocompleteField
add_introspection_rules ([], ["^tagging_autocomplete\.models\.TagAutocompleteField"])

# Create your models here.
comment_was_posted.connect(comment_notifier, sender=Comment)
class Tutoriales(models.Model):
    ''' Esta clase contendra el modelo para
        redactar los tutoriales ya sean nuevos
        o traidos de otro lugar
    '''
    fecha = models.DateField()
    titulo = models.CharField("Titulo del tutoriale", max_length=200)
    slug = models.SlugField(max_length = 200, unique = True,
                            help_text = 'unico Valor',editable=False)
    cuerpo = models.TextField()
    autor = models.ForeignKey(User)
    tag = TagAutocompleteField(help_text='SEPARAR LAS PALABRAS CON "," ')

    def __unicode__(self):
        return self.titulo
        
    def get_full_url(self):
        return "/tutoriales/tutorial/%s/" % self.slug
        
    def save(self, force_insert=False, force_update=False):
        try:
            Tutoriales.objects.get(pk=self.id)
        except:
            n = Tutoriales.objects.all().count()
            self.slug = str(n) + '-' + slugify(self.titulo)
        super(Tutoriales, self).save(force_insert, force_update)
    
    class Meta:
        verbose_name_plural = "Tutoriales"
	ordering = ['-fecha']
        
class Archivos(models.Model):
    nombre = models.CharField(max_length=200)
    archivo = models.FileField(upload_to=get_file_path, null=True, blank=True)
    libro = models.ForeignKey(Tutoriales)

    fileDir = 'attachments/archivos'    
    
    def __unicode__(self):
        return self.nombre
        
    class Meta:
        verbose_name_plural = "Subir Archivos"

from django.db.models import signals        
class TutorialesModerator(CommentModerator):
    email_notification = True
    auto_moderate_field= 'fecha'
    moderate_after = 10
    #auto_close_field = 'fecha'
    #close_after = 7

    def moderate(self, comment, content_object, request):
        already_moderated = super(TutorialesModerator,self).moderate(comment, content_object, request)
        if already_moderated:
            return True
        return False
        
signals.post_save.connect(comment_notifier, sender=Comment)    
moderator.register(Tutoriales, TutorialesModerator)
