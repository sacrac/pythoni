# -*- coding: UTF-8 -*-
from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from thumbs import ImageWithThumbsField
from pythoni.utils import get_file_path  

class Evento(models.Model):
    '''Modelo que representa el tipo de contenido Noticias'''
    titulo = models.CharField('Título', max_length = 120,
                               unique = True,blank = False, null = False)
    slug = models.SlugField(max_length = 120, unique = True,
                            help_text = 'unico Valor',editable=False)
    fecha_inicio = models.DateTimeField('Fecha de Inicio',blank = False, null = False,
            help_text='La hora debe presentarse en hora militar 13 = 1pm, 14 = 2pm etc..')
    fecha_final = models.DateTimeField('Fecha Final',blank = False, null = False, 
            help_text='La hora debe presentarse en hora militar 13 = 1pm, 14 = 2pm etc..')
    lugar = models.CharField('Lugar', max_length = 150,blank = True, null = True)
    contenido = models.TextField('Contenido',blank = True, null = True)
    foto = ImageWithThumbsField(upload_to=get_file_path,
                                         sizes=((150,150),(250,250)), null=True, blank=True)
    contacto = models.CharField(max_length=250, blank=True, null=True, help_text="Nombre y correo o numero de telefono del responsable" )
    fileDir = 'eventosfotos/fotos'
    def __unicode__(self):
        return self.titulo

    def get_full_url(self):
        return "/eventos/evento/%s/" % self.slug

    class Meta:
        verbose_name = "Evento"
        verbose_name_plural = "Eventos"
    
    def save(self, force_insert=False, force_update=False):
        try:
            Evento.objects.get(pk=self.id)
        except:
            n = Evento.objects.all().count()
            self.slug = str(n) + '-' + slugify(self.titulo)
        super(Evento, self).save(force_insert, force_update)
    #metodo url del objeto
    def get_full_url(self):
        return "/eventos/evento/%s/" % self.slug

    #metodo para obtener el nombre del objeto
    def get_name(self):
        return self.titulo
