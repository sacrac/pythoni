# Create your views here.
from django.http import Http404, HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404, render
from django.core.paginator import Paginator, InvalidPage, EmptyPage
import feedparser
from sitio.models import *
from evento.models import *
from twython import Twython
 
def get_feed_django():
    sites_feed = "https://www.djangoproject.com/rss/weblog/"
    feed = feedparser.parse(sites_feed)
    
    info = []
    for entry in feed.entries[:3]:
        info.append([entry.title,entry.link,entry.description,entry.updated])
    return info

def get_feed_python():
    sites_feed = "http://python.org/channews.rdf"
    feed = feedparser.parse(sites_feed)
  
    info = []
    for entry in feed.entries[:3]:
        info.append([entry.title,entry.link,entry.description,entry.updated])
    return info
    
def index(request):
    aggregate = get_feed_django()
    aggregate1 = get_feed_python()
    
    #ultimos tutorial
    tutorial = Tutoriales.objects.all()
    tuto = []
    for last in tutorial[:3]:
        tuto.append([last.slug,last.titulo,last.cuerpo,last.fecha])
        
    #ultimos eventos
    eventos = Evento.objects.all()
    events = []
    for last in eventos[:3]:
        events.append([last.slug,last.titulo,last.contenido,last.fecha_inicio])
        
    twitter = Twython() 
    #user_timeline = twitter.getUserTimeline(screen_name="pythonicaragua")
    ultimos = []
    #for tweet in user_timeline[:3]:
    #    ultimos.append([tweet["text"],tweet["created_at"]])

    return render(request, 'feed.html', locals())
    
def pagina_tutoriales(request):
    lista_tutorial = Tutoriales.objects.all()
    paginator = Paginator(lista_tutorial, 10)
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    try:
        tutoriales = paginator.page(page)
    except (EmptyPage, InvalidPage):
        tutoriales = paginator.page(paginator.num_pages)
        
    return render_to_response('sitio/tutoriales.html', locals(),
                                context_instance=RequestContext(request))
                                
def ver_tuto(request, slug):
    tutoriales = get_object_or_404(Tutoriales, slug = slug)
    return render_to_response('sitio/tutorial.html', locals(),
                              context_instance=RequestContext(request))   
