from django.contrib import admin
from sitio.models import *

class ArchivosInline(admin.TabularInline):
    model = Archivos
    extra = 1
    max_num = 6
    
class TutorialAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        obj.autor = request.user
        obj.save()
        
    def queryset(self, request):
        qs = super(TutorialAdmin, self).queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(autor=request.user)
        
    save_on_top = True
    actions_on_top = True
    inlines = [ArchivosInline]
    list_display = ('titulo','fecha', 'autor')
    list_filter = ['titulo',]
    search_fields = ['titulo', 'autor']
    date_hierarchy = 'fecha'
    
    class Media:
        js = ('js/tiny_mce/tiny_mce.js',
              'js/basic_config.js',)
    
admin.site.register(Tutoriales, TutorialAdmin)
